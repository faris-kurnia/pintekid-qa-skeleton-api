const frisby = require('frisby');
const env = require('../config');
const api_url = env.baseUrl;

frisby.globalSetup({
  request: {
    headers: {
      'AUTHORIZATION': 'MTI3LjAuMC4x',
      'Content-Type': 'application/json'
           
    }
  }
});

describe("Positif", function(){
	it('Happy Case', function (){
    return frisby
      .post(api_url, {
        "program_fee" : 10000000,
        "down_payment" :parseFloat("10"),
        "platform_fee" :parseFloat("5"),
        "admin_fee" : 10000,
        "mdr" : parseFloat("5"),
        "grace_period" : 3,
        "loan_duration" : 6,
        "dp_to_pintek"  : 1,
        "admin_to_pintek" : 1,
        "cof" : parseFloat("16"),
        "disburse_date" : "2000-01-07",
        "monthly_flat_rate" : parseFloat("2"),
        "monthly_flat_rate_grace_period" : parseFloat("1")
    })
    .expect('status', 200)
    .expect('header', 'content-type', /application\/json/)
    .expect('json', 'response', {
      principal : 9000000.0,
      loan_disbursed : 8500000.0,
      loan_disbursed_to_institution : 9500000.0,
      installment_duration : 3,
      monthly_repayment : 3180000.0,
      monthly_repayment_grace_period : 90000.0,
      actual_loan_principal : 9000000.0,
      actual_monthly_rate : 0.029710097561517535,
      consumer_apr : 0.35652117073821044,
      actual_mdr : 500000.0,
      actual_consumer_apr : 0.35652117073821044,
      monthly_rate : 0.029710097561517535,
      all_in_apr: 0.6302815204075789,
      issuance_revenue : 0.10666666666666667,
      monthly_payment : 3080353.0
    })
  })
  
  it('DP to Pintek Yes and Admin to Pintek No', function (){
    return frisby
      .post(api_url, {
        "program_fee" : 10000000,
        "down_payment" :parseFloat("10.11"),
        "platform_fee" :parseFloat("1.10"),
        "admin_fee" : 100000,
        "mdr" : parseFloat("5.1"),
        "grace_period" : 3,
        "loan_duration" : 6,
        "dp_to_pintek" : 1,
        "admin_to_pintek" : 0,
        "cof" : parseFloat("16.11"),
        "disburse_date" : "2020-01-07",
        "monthly_flat_rate" : parseFloat("2.1"),
        "monthly_flat_rate_grace_period" : parseFloat("1.1")
    })
    .expect('status', 200)
    .expect('header', 'content-type', /application\/json/)
    .expect('json', 'response', {
      installment_duration: 3
    })    
  })

  it('All in APR', function (){
    return frisby
      .post(api_url, {
        "program_fee" : 10000000,
        "down_payment" :parseFloat("10"),
        "platform_fee" :parseFloat("5"),
        "admin_fee" : 10000,
        "mdr" : parseFloat("5"),
        "grace_period" : 3,
        "loan_duration" : 6,
        "dp_to_pintek"  : 1,
        "admin_to_pintek" : 1,
        "cof" : parseFloat("16"),
        "disburse_date" : "2000-01-07",
        "monthly_flat_rate" : parseFloat("2"),
        "monthly_flat_rate_grace_period" : parseFloat("1")
    })
    .expect('status', 200)
    .expect('header', 'content-type', /application\/json/)
    .expect('json', 'response', {
      all_in_apr: 0.6302815204075789
    })   
  })
});

// it('Happy Case', function (){
//     return frisby
//       .post(api_url, {
//         "program_fee" : 10000000,
//         "down_payment" :parseFloat("10"),
//         "platform_fee" :parseFloat("5"),
//         "admin_fee" : 10000,
//         "mdr" : parseFloat("5"),
//         "grace_period" : 3,
//         "loan_duration" : 6,
//         "dp_to_pintek"  : 1,
//         "admin_to_pintek" : 1,
//         "cof" : parseFloat("16"),
//         "disburse_date" : "2000-01-07",
//         "monthly_flat_rate" : parseFloat("2"),
//         "monthly_flat_rate_grace_period" : parseFloat("1")
//     })
//     .expect('status', 200)
//     .expect('header', 'content-type', /application\/json/)
//     .expect('json', 'response', {
//       principal : 9000000.0,
//       loan_disbursed : 8500000.0,
//       loan_disbursed_to_institution : 9500000.0,
//       installment_duration : 3,
//       monthly_repayment : 3180000.0,
//       monthly_repayment_grace_period : 90000.0,
//       actual_loan_principal : 9000000.0,
//       actual_monthly_rate : 0.029710097561517535,
//       consumer_apr : 0.35652117073821044,
//       actual_mdr : 500000.0,
//       actual_consumer_apr : 0.35652117073821044,
//       monthly_rate : 0.029710097561517535,
//       all_in_apr: 0.6302815204075789,
//       fee_at_issuance : 960000.0,
//       issuance_revenue : 0.10666666666666667,
//       total_revenue : 0.12988222222222223,
//       monthly_payment : 3080353.0
//     })    
// });
// it('DP to Pintek Yes and Admin to Pintek No', function (){
//   return frisby
//     .post(api_url, {
//       "program_fee" : 10000000,
//       "down_payment" :parseFloat("10.11"),
//       "platform_fee" :parseFloat("1.10"),
//       "admin_fee" : 100000,
//       "mdr" : parseFloat("5.1"),
//       "grace_period" : 3,
//       "loan_duration" : 6,
//       "dp_to_pintek" : 1,
//       "admin_to_pintek" : 0,
//       "cof" : parseFloat("16.11"),
//       "disburse_date" : "2020-01-07",
//       "monthly_flat_rate" : parseFloat("2.1"),
//       "monthly_flat_rate_grace_period" : parseFloat("1.1")
//   })
//   .expect('status', 200)
//   .expect('header', 'content-type', /application\/json/)
//   .expect('json', 'response', {
//     installment_duration: 3
//   })    
// });
// it('All in APR', function (){
//   return frisby
//     .post(api_url, {
//       "program_fee" : 10000000,
//       "down_payment" :parseFloat("10"),
//       "platform_fee" :parseFloat("5"),
//       "admin_fee" : 10000,
//       "mdr" : parseFloat("5"),
//       "grace_period" : 3,
//       "loan_duration" : 6,
//       "dp_to_pintek"  : 1,
//       "admin_to_pintek" : 1,
//       "cof" : parseFloat("16"),
//       "disburse_date" : "2000-01-07",
//       "monthly_flat_rate" : parseFloat("2"),
//       "monthly_flat_rate_grace_period" : parseFloat("1")
//   })
//   .expect('status', 200)
//   .expect('header', 'content-type', /application\/json/)
//   .expect('json', 'response', {
//     all_in_apr: 0.6302815204075789
//   })   
// });