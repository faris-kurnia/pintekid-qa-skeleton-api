const frisby = require('frisby');
const env = require('../config');
const api_url = env.baseUrl;

frisby.globalSetup({
  request: {
    headers: {
      'AUTHORIZATION': 'MTkyLjE2OC40My4xOTc=',
      'Content-Type': 'application/json',      
    }
  }
});
it('down_payment cannot be higher than program_fee', function (){
  return frisby
    .post(api_url, {
      "program_fee" : 0,
      "down_payment" :parseFloat("50.33"),
      "platform_fee" :parseFloat("5.1"),
      "admin_fee" : 100000,
      "mdr" : parseFloat("5.1"),
      "grace_period" : 2,
      "loan_duration" : 5,
      "dp_to_pintek" : 1,
      "admin_to_pintek" : 1,
      "cof" : parseFloat("16.11"),
      "disburse_date" : "2019-07-31",
      "monthly_flat_rate" : parseFloat("1.1"),
      "monthly_flat_rate_grace_period" : parseFloat("2.1")
  })
  .expect('json', 'headers', {
    status: 5001,
    message : 'Parameter down_payment cannot be higher than program_fee.'
  }) 
});
it('down payment more than 80 persen', function (){
  return frisby
    .post(api_url, {
      "program_fee" : 10000000,
      "down_payment" :parseFloat("81.01"),
      "platform_fee" :parseFloat("10.01"),
      "admin_fee" : 100000,
      "mdr" : parseFloat("5.1"),
      "grace_period" : 2,
      "loan_duration" : 5,
      "dp_to_pintek" : 1,
      "admin_to_pintek" : 1,
      "cof" : parseFloat("16.11"),
      "disburse_date" : "2020-01-05",
      "monthly_flat_rate" : parseFloat("1.1"),
      "monthly_flat_rate_grace_period" : parseFloat("2.1")
    })
      .expect('json', 'headers', {
        status: 5002,
        message : 'Parameter down_payment cannot be exceeding 80% of program_fee.'
      }) 
});
it('Parameter grace_period cannot be higher than loan_duration', function (){
  return frisby
    .post(api_url, {
      "program_fee" : 10000000,
      "down_payment" :parseFloat("80.0"),
      "platform_fee" :parseFloat("10.0"),
      "admin_fee" : 100000,
      "mdr" : parseFloat("5.1"),
      "grace_period" : 6,
      "loan_duration" : 5,
      "dp_to_pintek" : 1,
      "admin_to_pintek" : 1,
      "cof" : parseFloat("16.11"),
      "disburse_date" : "2020-01-05",
      "monthly_flat_rate" : parseFloat("1.1"),
      "monthly_flat_rate_grace_period" : parseFloat("2.1")
    })
      .expect('json', 'headers', {
        status: 5004,
        message : 'Parameter grace_period cannot be higher than or equal to loan_duration.'
      }) 
});
it('Missing required parameter', function (){
  return frisby
    .post(api_url, {
      "program_fee" : 10000000,
      "platform_fee" :parseFloat("10.0"),
      "admin_fee" : 100000,
      "mdr" : parseFloat("5.1"),
      "grace_period" : 6,
      "loan_duration" : 5,
      "dp_to_pintek" : 1,
      "admin_to_pintek" : 1,
      "cof" : parseFloat("16.11"),
      "disburse_date" : "2020-01-05",
      "monthly_flat_rate" : parseFloat("1.1"),
      "monthly_flat_rate_grace_period" : parseFloat("2.1")
    })
      .expect('json', 'headers', {
        status: 5005,
        message : 'Missing required parameter(s):down_payment.'
      }) 
});
it('Unknown error', function (){
  return frisby
    .post(api_url, {
      "program_fee" : "10000000",
      "down_payment" :parseFloat("80.0"),
      "platform_fee" :parseFloat("10.0"),
      "admin_fee" : 100000,
      "mdr" : parseFloat("5.1"),
      "grace_period" : 6,
      "loan_duration" : 5,
      "dp_to_pintek" : 1,
      "admin_to_pintek" : 1,
      "cof" : parseFloat("16.11"),
      "disburse_date" : "2020-01-05",
      "monthly_flat_rate" : parseFloat("1.1"),
      "monthly_flat_rate_grace_period" : parseFloat("2.1")
    })
      .expect('json', 'headers', {
        status: 5000,
        message : 'Parameter program_fee should be Integer'
      }) 
});
