const frisby = require('frisby');
const env = require('../config');
const TG_url = env.instanTGUrl;

frisby.globalSetup({
    request: {
      headers: {
        'Content-Type': 'application/json',      
      }
    }
  });

  describe("Positif 10 data", function (){
    //   it("start from 0", function (){
    //     return frisby
    //         .post (TG_url, {
    //             "total_approved_all_b2c" : "0",
    //             "total_approved_test_group" : "0"
    //         })
    //         .expect('status', 200)
    //         .expect('header', 'content-type', /application\/json/)
    //         .expect('json', {
    //             passed : false
    //         })
    //   })

      it("start from 1", function (){
        return frisby
            .post (TG_url, {
                "total_approved_all_b2c" : "1",
                "total_approved_test_group" : "0"
            })
            .expect('status', 200)
            .expect('header', 'content-type', /application\/json/)
            .expect('json', {
                passed : false
            })
      })

      it("start from 2", function (){
        return frisby
            .post (TG_url, {
                "total_approved_all_b2c" : "2",
                "total_approved_test_group" : "0"
            })
            .expect('status', 200)
            .expect('header', 'content-type', /application\/json/)
            .expect('json', {
                passed : false
            })
      })

      it("start from 3", function (){
        return frisby
            .post (TG_url, {
                "total_approved_all_b2c" : "3",
                "total_approved_test_group" : "0"
            })
            .expect('status', 200)
            .expect('header', 'content-type', /application\/json/)
            .expect('json', {
                passed : false
            })
      })

      it("start from 4", function (){
        return frisby
            .post (TG_url, {
                "total_approved_all_b2c" : "4",
                "total_approved_test_group" : "0"
            })
            .expect('status', 200)
            .expect('header', 'content-type', /application\/json/)
            .expect('json', {
                passed : false
            })
      })

      it("start from 5", function (){
        return frisby
            .post (TG_url, {
                "total_approved_all_b2c" : "5",
                "total_approved_test_group" : "0"
            })
            .expect('status', 200)
            .expect('header', 'content-type', /application\/json/)
            .expect('json', {
                passed : false
            })
      })

      it("start from 6", function (){
        return frisby
            .post (TG_url, {
                "total_approved_all_b2c" : "6",
                "total_approved_test_group" : "0"
            })
            .expect('status', 200)
            .expect('header', 'content-type', /application\/json/)
            .expect('json', {
                passed : false
            })
      })

      it("start from 7", function (){
        return frisby
            .post (TG_url, {
                "total_approved_all_b2c" : "7",
                "total_approved_test_group" : "0"
            })
            .expect('status', 200)
            .expect('header', 'content-type', /application\/json/)
            .expect('json', {
                passed : false
            })
      })

      it("start from 8", function (){
        return frisby
            .post (TG_url, {
                "total_approved_all_b2c" : "8",
                "total_approved_test_group" : "0"
            })
            .expect('status', 200)
            .expect('header', 'content-type', /application\/json/)
            .expect('json', {
                passed : false
            })
      })

      it("start from 9", function (){
        return frisby
            .post (TG_url, {
                "total_approved_all_b2c" : "9",
                "total_approved_test_group" : "0"
            })
            .expect('status', 200)
            .expect('header', 'content-type', /application\/json/)
            .expect('json', {
                passed : false
            })
      })

      it("start from 10", function (){
        return frisby
            .post (TG_url, {
                "total_approved_all_b2c" : "10",
                "total_approved_test_group" : "0"
            })
            .expect('status', 200)
            .expect('header', 'content-type', /application\/json/)
            .expect('json', {
                passed : true
            })
      })

  })