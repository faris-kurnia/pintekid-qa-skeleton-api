const frisby = require('frisby');
const env = require('../config');
const api_url = env.baseUrl;

frisby.globalSetup({
  request: {
    headers: {
      'AUTHORIZATION': 'MTkyLjE2OC40My4xOTc=',
      'Content-Type': 'application/json',      
    }
  }
});

  for(let i = 0; i < 5; i++) {
    it('Happy Case 1'+i,async function (){

    await frisby
      .post(api_url, {
        "program_fee" : 20000000,
        "down_payment" :parseFloat("50.33"),
        "platform_fee" :parseFloat("5.1"),
        "admin_fee" : 100000,
        "mdr" : parseFloat("5.1"),
        "grace_period" : 2,
        "loan_duration" : 5,
        "dp_to_pintek" : 1,
        "admin_to_pintek" : 1,
        "cof" : parseFloat("16.11"),
        "disburse_date" : "2019-07-31",
        "monthly_flat_rate" : parseFloat("1.1"),
        "monthly_flat_rate_grace_period" : parseFloat("2.1")
    })
    .expect('status', 200)
    .expect('header', 'content-type', /application\/json/)
    .expect('json', 'response', {
      installment_duration: 3,
      principal : 9934000
    })  
  return Promise.resolve();
  });
}